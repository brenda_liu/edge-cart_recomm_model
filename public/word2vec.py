def cosine_similarity(vec1, vec2):
    dot_product = sum(v1 * v2 for v1, v2 in zip(vec1, vec2))
    magnitude1 = sum(v ** 2 for v in vec1) ** 0.5
    magnitude2 = sum(v ** 2 for v in vec2) ** 0.5
    return dot_product / (magnitude1 * magnitude2)

def find_similar_words(keyword, word_vectors):
    keyword = keyword.lower()
    print(keyword)
    if keyword not in word_vectors:
        return ['Word not found in vocabulary.']

    similarities = [
        (word, cosine_similarity(word_vectors[keyword], vec))
        for word, vec in word_vectors.items()
    ]

    similarities.sort(key=lambda item: item[1], reverse=True)
    return similarities[:5]  # Top 5 similar words
