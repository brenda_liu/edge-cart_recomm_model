export function cosine_similarity(vec1, vec2) {
    const dot_product = vec1.reduce((acc, v1, i) => acc + v1 * vec2[i], 0);
    const magnitude1 = Math.sqrt(vec1.reduce((acc, v) => acc + v ** 2, 0));
    const magnitude2 = Math.sqrt(vec2.reduce((acc, v) => acc + v ** 2, 0));
    return dot_product / (magnitude1 * magnitude2);
}

export function find_similar_words(keyword, word_vectors) {
    keyword = keyword.toLowerCase();
    console.log(keyword);
    if (!(keyword in word_vectors)) {
        return ['Word not found in vocabulary.'];
    }

    const similarities = Object.entries(word_vectors).map(([word, vec]) => {
        return [word, cosine_similarity(word_vectors[keyword], vec)];
    });

    similarities.sort((a, b) => b[1] - a[1]);
    return similarities.slice(0, 5); // Top 5 similar words
}
